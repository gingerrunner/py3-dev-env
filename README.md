# py3-dev-env

Python 3 development environment based on a minimal Linux image.

## Building

make image

## Running

```
docker run -it -v $PWD:/workspace cmiller/py3-dev-env 
```

