OUTPUT_IMAGE=cmiller/py3-dev-env

.PHONY: all image 
IMAGE_TAG=$(shell cat VERSION | tr -d '\r\n')

image:
	docker build -t $(OUTPUT_IMAGE):$(IMAGE_TAG) .

