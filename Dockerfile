FROM debian:bookworm-slim

ENV DEBIAN_FRONTEND=noninteractive

USER root

RUN apt-get update && apt-get install -y \
    build-essential \
    python3 && \
    # https://stackoverflow.com/questions/75608323/how-do-i-solve-error-externally-managed-environment-every-time-i-use-pip-3
    mv /usr/lib/python3.11/EXTERNALLY-MANAGED /usr/lib/python3.11/EXTERNALLY-MANAGED.old && \
    apt-get install python3-pip -y && \
    pip install pyinstaller && \
    rm -rf /var/lib/apt/lists/*

RUN mkdir -m 777 /workspace

RUN useradd -ms /bin/bash dev
USER dev
WORKDIR /workspace
CMD ["/bin/bash"]

